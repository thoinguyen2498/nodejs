var express = require('express');
var bodyParse = require('body-parser');

var app = express();
var url = bodyParse.urlencoded({extended: true});

app.set('view engine','ejs');

app.use('/assets',express.static('stuff'));

app.get('/', function(req,res){
    res.sendFile(__dirname + '/index.html','utf8');
});

app.get('/hello/:name', function(req,res){
    var infor ={
        name: 'Nguyen Van A',
        age: '22',
        email: 'anguyen@gmail.com',
        hobbies: ['eat','sleep','code'],
    };

    res.render('hello',{name: req.params.name,information: infor});
})

app.get('/abc',function(req,res){
    // console.log(req.query);
    res.render('abc',{data: req.query});
})

//Lấy dữ liệu phương thức post
app.post('/abc', url ,function(req,res){
    console.log(req.body);
    res.render('abc-post',{data: req.body});
})

app.listen(8888);