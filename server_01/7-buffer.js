// CHuyển dữ liệu sang dạng buffer.

var buffer = new Buffer("hello", "utf-8");
console.log(buffer);

// Chuyển buffer sang string.

console.log(buffer.toString());

// Chuyển buffer sang json;
console.log(buffer.toJSON());

// doc file
var fs = require('fs');

var noidung = fs.readFileSync( __dirname + "/danhsach.txt");

console.log(noidung.toString());