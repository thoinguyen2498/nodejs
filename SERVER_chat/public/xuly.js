var socket = io("http://localhost:3000");

socket.on("server-send-check-false",function(){
	alert("Đăng kí thất bại! (username người đã đăng kí)");
});
socket.on("server-send-check-true",function(data){
	$("#user").html(data);
	$("#loginForm").hide(2000);
	$("#chatForm").show(1000);
})
socket.on("server-send-all-ds",function(data){
	$("#boxContent").html("");
	data.forEach(function(i){
		$("#boxContent").append("<div class= 'user'>"+i+"</div>")
	});
})
socket.on("server-send-message",function(data){
	$("#listMessage").append("<div class='ms'>"+data.username+":"+data.content);
});

socket.on("co-nguoi-dang-go-chu",function(data){
	$("#thongBao").html(data);
})
socket.on("khong-go-chu",function(data){
	$("#thongBao").html("");
})

$( document ).ready(function() {
	$("#loginForm").show();
	$("#chatForm").hide();

	$("#btnDangKi").click(function(){
		socket.emit("client-send-username",$("#txtUsername").val());

	})
	$("#btnLogOut").click(function(){
		socket.emit("log-out");
		$("#loginForm").show(1000);
		$("#chatForm").hide(2000);
	})
	$("#btnSenMessage").click(function(){
		socket.emit("user-send-message",$("#txtMessege").val());
	})

	$("#txtMessege").focusin(function(){
		socket.emit("dang-go-chu");
	})
	$("#txtMessege").focusout(function(){
		socket.emit("stop-go-chu");
	})
});
