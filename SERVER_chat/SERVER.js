var express = require("express");
var app = express();
app.use(express.static("public"));
app.set("view engine","ejs");
app.set("views","./views");

var server = require("http").Server(app);
var io = require("socket.io")(server);
server.listen(3000);

var mangUsers=["aaa"];

io.on("connection",function(socket){
    console.log("Co nguoi ket noi: " +socket.id);
    socket.on("client-send-username", function(data){
        if (mangUsers.indexOf(data)>=0) {
            //fail
            socket.emit("server-send-check-false");
        }else{
            //chèn thêm phần tử vào mảng
            mangUsers.push(data);
            //Thêm thuộc tính username vào socket
            socket.Username = data;
            socket.emit("server-send-check-true",data);
            io.sockets.emit("server-send-all-ds",mangUsers);
        }
    });
    socket.on("log-out",function(){
        mangUsers.splice(
            mangUsers.indexOf(socket.Username), 1
        );
        socket.broadcast.emit("server-send-all-ds",mangUsers);
    })
    socket.on("user-send-message",function(data){
        io.sockets.emit("server-send-message",{username:socket.Username, content:data});
    })
    socket.on("dang-go-chu",function(){
        var s =socket.Username +"dang go chu";
        io.sockets.emit("co-nguoi-dang-go-chu",s);
    })
    socket.on("stop-go-chu",function(){
        io.sockets.emit("khong-go-chu");
    })
    socket.on("mua-hang",function (data) {
        socket.username=data;
        io.sockets.in("admin").emit("user-mua-hang",data);
        console.log("user mua hang");
    })
    socket.on("admin",function(){
        socket.join("admin");
        console.log("admin online");
    })
    socket.on("da nghe",function(data){
        console.log("admin da nghe");
        console.log(data);
    })
});

app.get("/",function(req, res) {
    res. render("trangchu");
});